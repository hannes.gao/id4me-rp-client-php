<?php

namespace Id4me\RP\Model;

use Id4me\RP\Helper\Base64UrlHelper;
use phpseclib\Crypt\RSA;
use phpseclib\Math\BigInteger;

/**
 * Class IdToken
 *
 * @package Id4me\RP\Model
 */
class IdToken
{
    /**
     * @var string
     */
    protected $headerOriginal;

    /**
     * @var array
     */
    protected $headerDecoded;

    /**
     * @var string
     */
    protected $bodyOriginal;

    /**
     * @var array
     */
    protected $bodyDecoded;

    /**
     * @var string
     */
    protected $signatureOriginal;

    /**
     * @var string
     */
    protected $signatureDecoded;

    /**
     * IdToken constructor.
     * 
     * @param string $idToken
     */
    function __construct($idToken = null)
    {
        if (! is_null($idToken)) {
            $idTokenParts = explode('.', $idToken);

            if (count($idTokenParts) == 3) {
                list(
                    $this->headerOriginal,
                    $this->bodyOriginal,
                    $this->signatureOriginal
                ) = $idTokenParts;

                // Decode token parts
                $this->headerDecoded = json_decode(Base64UrlHelper::base64url_decode($this->headerOriginal), true);
                $this->bodyDecoded = json_decode(Base64UrlHelper::base64url_decode($this->bodyOriginal), true);
                $this->signatureDecoded = Base64UrlHelper::base64url_decode($this->signatureOriginal);
            }
        }
    }

    /**
     * @return string
     */
    public function getOriginalHeader()
    {
        return $this->headerOriginal;
    }

    /**
     * @return array
     */
    public function getDecodedHeader()
    {
        return $this->headerDecoded;
    }

    /**
     * @return string
     */
    public function getOriginalBody()
    {
        return $this->bodyOriginal;
    }

    /**
     * @return array
     */
    public function getDecodedBody()
    {
        return $this->bodyDecoded;
    }

    /**
     * @return string
     */
    public function getOriginalSignature()
    {
        return $this->signatureOriginal;
    }

    /**
     * @return string
     */
    public function getDecodedSignature()
    {
        return $this->signatureDecoded;
    }

    /**
     * @param string $property
     *
     * @return string
     */
    public function getHeaderValue($property)
    {
        return $this->fetchArrayPropertyValue($this->headerDecoded, $property);
    }

    /**
     * @param string $property
     *
     * @return string
     */
    public function getBodyValue($property)
    {
        return $this->fetchArrayPropertyValue($this->bodyDecoded, $property);
    }

    /**
     * Generates the OpenSSL key  with the given authority RSA key
     * 
     * @param array $keyDetails
     * 
     * @return resource|bool
     */
    public function getOpenSslKey(array $keyDetails)
    {
        if (! isset($keyDetails['n']) || ! isset($keyDetails['n'])) {
            return false;
        }
        
        $crypt = new RSA();

        $n = new BigInteger(Base64UrlHelper::base64url_decode($keyDetails['n']), 256);
        $e = new BigInteger(Base64UrlHelper::base64url_decode($keyDetails['e']), 256);

        $crypt->loadKey(['e' => $e, 'n' => $n]);
        $pem_string = $crypt->getPublicKey();
        
        return openssl_pkey_get_public($pem_string);
    }

    /**
     * Extracts token property value out of given token data list if found
     *
     * @param array  $source
     * @param string $property
     *
     * @return mixed
     */
    private function fetchArrayPropertyValue(array $source, string $property)
    {
        $result = null;
        
        if (array_key_exists($property, $source)) {
            $result = $source[$property];
        }

        return $result;
    }
}
