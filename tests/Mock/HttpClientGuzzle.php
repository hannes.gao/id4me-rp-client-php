<?php

namespace Id4me\Test\Mock;

use GuzzleHttp\Client;
use Id4me\RP\HttpClient;

class HttpClientGuzzle implements HttpClient {

    public function get($url, array $headers = [])
    {
        $httpClient = new Client();
        $result = $httpClient->get($url);

        return $result->getBody()->getContents();

    }

    public function post($url, $body, array $headers = [])
    {
        $httpClient = new Client();

        $body = [
            'headers' => $headers,
            'body' => $body
        ];

        $result = $httpClient->post($url, $body);

        return $result->getBody()->getContents();
    }
}

